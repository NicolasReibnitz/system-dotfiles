#!/bin/bash

function fixIt() {
	echo ""
	echo "Step 1: ´git commit -m 'Preparing .gitignore fix'´..."
	git commit -m 'Preparing .gitignore fix'
	echo ""
	echo "Step 2: ´git rm -r --cached .´..."
	git rm -r --cached .
	echo ""
	echo "Step 3: ´git add .´..."
	git add .
	echo ""
	echo "Step 4: ´git commit -m '.gitignore fix'´..."
	git commit -m '.gitignore fix'
	echo ""
	echo ""
	echo "Done!"
	echo ""
}

echo "This script will untrack files already added to git repository based on"
echo ".gitignore."
echo ""
echo "Step 1: It will make sure all your changes are committed, including your"
echo "        .gitignore file:"
echo "        ´git commit -m 'Preparing .gitignore fix'´"
echo "Step 2: Remove everything from the repository. To clear your repo, it uses:"
echo "        ´git rm -r --cached .´"
echo "Step 3: We re add everything using:"
echo "        ´git add .´"
echo "Step 4: We commit again:"
echo "        ´git commit -m '.gitignore fix'´"
echo ""
echo "Is this what you want? Press 1 or 2, followed by enter."

select yn in "Yes" "No"; do
	case $yn in
	Yes)
		fixIt
		break
		;;
	No) exit ;;
	esac
done
