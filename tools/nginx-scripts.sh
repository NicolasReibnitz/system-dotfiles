#!/bin/bash

alias nginxreload="sudo nginx -s reload"
alias nginxrestart="sudo nginx -s stop && sudo nginx"
alias nginxservers="cd /opt/homebrew/etc/nginx/servers"
alias nginxlist="ll /opt/homebrew/etc/nginx/servers"

# nginxcreate text.x  /Users/yourname/Code/laravel/public/
function nginxcreate() {
	wget https://gist.githubusercontent.com/kevindees/deb3e2bdef377bbf2ffacbc48dfa7574/raw/1d5dc055fe87319a7f247808c9f9ee14c6abd9cd/nginx-server-template-m1.conf -O /opt/homebrew/etc/nginx/servers/"$1".conf
	sed -i '' "s:{{host}}:$1:" /opt/homebrew/etc/nginx/servers/"$1".conf

	if [ "$2" ]; then
		sed -i '' "s:{{root}}:$2:" /opt/homebrew/etc/nginx/servers/"$1".conf
	else
		sed -i '' "s:{{root}}:$HOME/Sites/$1:" /opt/homebrew/etc/nginx/servers/"$1".conf
	fi

	nginxaddssl "$1"

	nginxrestart

	code /opt/homebrew/etc/nginx/servers/"$1".conf
}

function nginxaddssl() {
	openssl req \
		-x509 -sha256 -nodes -newkey rsa:2048 -days 3650 \
		-subj "/CN=$1" \
		-reqexts SAN \
		-extensions SAN \
		-config <(
			cat /System/Library/OpenSSL/openssl.cnf
			printf "[SAN]\nsubjectAltName=DNS:$1"
		) \
		-keyout /opt/homebrew/etc/nginx/ssl/"$1".key \
		-out /opt/homebrew/etc/nginx/ssl/"$1".crt

	sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain /opt/homebrew/etc/nginx/ssl/"$1".crt
}

function nginxedit() {
	code /opt/homebrew/etc/nginx/servers/"$1"
}
