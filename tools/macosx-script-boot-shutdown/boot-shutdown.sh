#!/bin/bash

#
# Author: Vincenzo D'Amore v.damore@gmail.com
# 20/11/2014
#

function shutdown() {
	printf "%s\t%s\tReceived a signal to shutdown....\n" "$(date)" "$(whoami)"

	# INSERT HERE THE COMMAND YOU WANT EXECUTE AT SHUTDOWN

	# REMEMBER THAT THE USER WILL BE ROOT! $HOME or ~ WILL NOT BE YOU!

	# CLEAR SPOTIFY CACHE IN ORDER FOR IT TO START A LOT FASTER
	rm -rf "/Users/nicolasreibnitz/Library/Caches/com.spotify.client"
	rm -rf "/Users/nicolasreibnitz/Library/Application Support/Spotify/PersistentCache"

	exit 0
}

function startup() {
	printf "%s\t%s\tStarting....\n" "$(date)" "$(whoami)"

	# INSERT HERE THE COMMAND YOU WANT EXECUTE AT STARTUP

	tail -f /dev/null &
	wait $!
}

trap shutdown SIGTERM
trap shutdown SIGKILL

startup
