#!/bin/bash

# -w flag permanently adds the plist to the Launch Daemon
# sudo launchctl load -w ~/Library/LaunchAgents/com.daslaboratory.startup.plist

# -w flag permanently remove the plist to the Launch Daemon
# sudo launchctl unload -w ~/Library/LaunchAgents/com.daslaboratory.startup.plist

if [ -e "/Users/nicolasreibnitz/Library/Application Support/Code/User/sync.lock" ]; then
	rm -f "/Users/nicolasreibnitz/Library/Application Support/Code/User/sync.lock"
	echo "$(date) $(whoami) > sync.lock file removed." >>/Users/nicolasreibnitz/system-dotfiles/logs/startup_agent.log
fi
