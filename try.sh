#!/bin/bash

function parse_git_dirty() {
	local STATUS
	local -a FLAGS
	FLAGS=('--porcelain' '--ignore-submodules=dirty')
	if [[ "$(command git config --get oh-my-zsh.hide-dirty)" != "1" ]]; then
		if [[ "$DISABLE_UNTRACKED_FILES_DIRTY" == "true" ]]; then
			FLAGS+=('--untracked-files=no')
		fi
		STATUS=$(command git status "${FLAGS[@]}" 2>/dev/null | tail -n1)
	fi
	if [[ -n $STATUS ]]; then
		echo "#"
	fi
}

function find_dirty_repositories() {
	echo "$(date) $(whoami) Repository commit check:" >"/Users/nicolasreibnitz/logs/dirty_repositories.log"
	{
		echo " "
		echo "DIRTY REPOSITORIES:"
	} >>"/Users/nicolasreibnitz/logs/dirty_repositories.log"

	while IFS= read -r dir_name; do
		cd "$dir_name" || exit
		if [[ "$(parse_git_dirty)" == '#' ]]; then
			echo "$dir_name"
		fi
	done < <(find "/Users/nicolasreibnitz/Documents/git/__interactive" -type d -iname 'node_modules' -prune -o -type d -path '*/.git' -prune -exec dirname {} \; | sort -f) >>"/Users/nicolasreibnitz/logs/dirty_repositories.log"
}
