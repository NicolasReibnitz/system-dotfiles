# Install

Clone repository to ~/system-dotfiles

````
git clone git@bitbucket.org:NicolasReibnitz/system-dotfiles.git ~/system-dotfiles
````

and add the following line to ~/.zshrc:

```
source ~/system-dotfiles/.index
```

Done!
