#!/bin/bash

# Author: Vincenzo D'Amore v.damore@gmail.com
# 20/11/2014
# https://github.com/freedev/macosx-script-boot-shutdown
#
# Note: fill in information into 'boot-shutdown-script.plist' and then copy it to /Library/LaunchDaemons using:
# 		$ sudo cp boot-shutdown-script.plist /Library/LaunchDaemons

function shutdown() {
	echo "$(date) $(whoami) Received a signal to shutdown"

	# INSERT HERE THE COMMAND YOU WANT EXECUTE AT SHUTDOWN
	find_dirty_repositories

	exit 0
}

function startup() {
	echo "$(date) $(whoami) Starting..."

	# INSERT HERE THE COMMAND YOU WANT EXECUTE AT STARTUP
	find_dirty_repositories
	open "/Users/nicolasreibnitz/logs/dirty_repositories.log"
	tail -f /dev/null &
	wait $!
}

function parse_git_dirty() {
	local STATUS
	local -a FLAGS
	FLAGS=('--porcelain' '--ignore-submodules=dirty')
	if [[ "$(command git config --get oh-my-zsh.hide-dirty)" != "1" ]]; then
		if [[ "$DISABLE_UNTRACKED_FILES_DIRTY" == "true" ]]; then
			FLAGS+=('--untracked-files=no')
		fi
		STATUS=$(command git status "${FLAGS[@]}" 2>/dev/null | tail -n1)
	fi
	if [[ -n $STATUS ]]; then
		echo "#"
	fi
}

function find_dirty_repositories() {
	echo "$(date) $(whoami) Repository commit check:" >"/Users/nicolasreibnitz/logs/dirty_repositories.log"
	{
		echo " "
		echo "DIRTY REPOSITORIES:"
	} >>"/Users/nicolasreibnitz/logs/dirty_repositories.log"

	while IFS= read -r dir_name; do
		cd "$dir_name" || exit
		if [[ "$(parse_git_dirty)" == '#' ]]; then
			echo "$dir_name"
		fi
	done < <(find "/Users/nicolasreibnitz/Documents/git/__interactive" -type d -iname 'node_modules' -prune -o -type d -path '*/.git' -prune -exec dirname {} \; | sort -f) >>"/Users/nicolasreibnitz/logs/dirty_repositories.log"
	exit 0
}

trap shutdown SIGTERM
trap shutdown SIGKILL

startup
